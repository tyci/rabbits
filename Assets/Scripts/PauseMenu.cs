﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject MainMenuUI;
    public GameObject optionsUI;
    public GameObject shopUI;
    public GameObject skinsUI;
    public GameObject UI;
    public Image EndScreen;
    public Image endAd;

    public GameObject RabbitH;
    public GameObject ChickH;
    public GameObject DuckH;
    public GameObject GorillaH;
    public GameObject PenguinH;
    public GameObject SlothH;
    public GameObject WalrusH;

    protected GameObject player;
    protected Potion potion;

    public TextMeshProUGUI SpeedText;
    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI CarrotText;
    public TextMeshProUGUI gold;
    public TextMeshProUGUI TimerText;

    public Button SpeedButton;
    public Button CarrotButton;
    public Button TimeButton;

    public Toggle joystickButton;
    public Toggle musicButton;
    public Toggle soundButton;

    protected GameController game;
    public RectTransform joystick;

    public AudioSource Sound;
    public AudioSource Music;

    public AdBanner ad;
    public Ads ads;
    public bool UsedShop = false;

    void Start()
    {
        ad = GetComponent<AdBanner>();
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        ads = game.GetComponent<Ads>();
        player = GameObject.FindGameObjectWithTag("Player");
        Music = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<AudioSource>();
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<RectTransform>();
        UI.SetActive(false);
        Pause();
        ad.StopAllCoroutines();
        ad.Stop();
        pauseMenuUI.SetActive(false);
        potion = FindObjectOfType<Potion>();
        Music.GetComponent<MusicControl>().Music1();
        //Options
        if (PlayerPrefs.GetInt("Joystick") == 0)
        {
            joystick.localPosition = new Vector2(120,joystick.localPosition.y);
            joystickButton.isOn = false;
        }
        else
        {
            joystick.localPosition = new Vector2(815, joystick.localPosition.y);
            joystickButton.isOn = true;
        }
        if (PlayerPrefs.GetInt("Music") == 1)
        {
            musicButton.isOn = false;
            Music.enabled = false;
        }
        else
        {
            musicButton.isOn = true;
            Music.enabled = true;
        }
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            soundButton.isOn = false;
            Sound.enabled = false;
        }
        else
        {
            soundButton.isOn = true;
            Sound.enabled = true;
        }
        CarrotText.text = "CARROTS (" + (1 + PlayerPrefs.GetFloat("Carrot") / .5f) + ")";
        TimeText.text = "TIME (" + (1 + PlayerPrefs.GetInt("Time")) + ")";
        SpeedText.text = "SPEED (" + (1 + PlayerPrefs.GetInt("Speed") / 200) + ")";
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
        SpeedButton.GetComponentInChildren<TextMeshProUGUI>().text = (15 + PlayerPrefs.GetInt("priceS")).ToString();
        TimeButton.GetComponentInChildren<TextMeshProUGUI>().text = (15 + PlayerPrefs.GetInt("priceT")).ToString();
        CarrotButton.GetComponentInChildren<TextMeshProUGUI>().text = (25 + PlayerPrefs.GetInt("priceC")).ToString();

        if (PlayerPrefs.GetInt("Gold") < 15+ PlayerPrefs.GetInt("priceS")) SpeedButton.interactable = false;
        else SpeedButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceT")) TimeButton.interactable = false;
        else TimeButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 25 + PlayerPrefs.GetInt("priceC")) CarrotButton.interactable = false;
        else CarrotButton.interactable = true;
    }

    void Update()
    {
        //return button
        if (Input.GetKeyDown(KeyCode.Escape) && optionsUI.activeSelf == true) CloseOptions();
        else if (Input.GetKeyDown(KeyCode.Escape) && skinsUI.activeSelf == true) CloseSkins();
        else if (Input.GetKeyDown(KeyCode.Escape) && shopUI.activeSelf == true) CloseShop();
        else if (Input.GetKeyDown(KeyCode.Escape) && MainMenuUI.activeSelf == true) Application.Quit();
        else if (Input.GetKeyDown(KeyCode.Escape) && pauseMenuUI.activeSelf == true) Resume();
        else if (Input.GetKeyDown(KeyCode.Escape)) Pause();

    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        UI.SetActive(true);
        Time.timeScale = 1;
        MonoBehaviour[] scripts = player.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = true;
        }
        ad.Stop();
    }
    public void Pause()
    {
        UI.SetActive(false);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        MonoBehaviour[] scripts = player.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
        ad.StartCoroutine("ShowBannerWhenReady");
    }
    public void OpenOptions()
    {
        optionsUI.SetActive(true);
    }
    public void CloseOptions()
    {
        optionsUI.SetActive(false);
    }
    public void OpenSkins()
    {
        ad.StartCoroutine("ShowBannerWhenReady");
        skinsUI.SetActive(true);
    }
    public void CloseSkins()
    {
        skinsUI.SetActive(false);
        if (UsedShop)
            Restart();
        ad.Stop();
    }
    public void OpenShop()
    {
        ad.StartCoroutine("ShowBannerWhenReady");
        shopUI.SetActive(true);
    }
    public void CloseShop()
    {
        shopUI.SetActive(false);
        if(UsedShop)
         Restart();
        UsedShop = false;
        ad.Stop();
    }
    public void EndAd()
    {
        ad.Stop();
        StartCoroutine("Timer");
        UI.SetActive(false);
        endAd.gameObject.SetActive(true);
    }
    public void EndAd2()
    {
        endAd.gameObject.SetActive(false);
        GameData.continueused = true;
        game.End();
    }
    public void End()
    {
        ad.Stop();
        StopAllCoroutines();
        switch (PlayerPrefs.GetInt("SelectedSkin"))
        {
            case 0:
                RabbitH.SetActive(true);
                break;
            case 1:
                ChickH.SetActive(true);
                break;
            case 2:
                DuckH.SetActive(true);
                break;
            case 3:
                PenguinH.SetActive(true);
                break;
            case 4:
                GorillaH.SetActive(true);
                break;
            case 5:
                SlothH.SetActive(true);
                break;
            case 6:
                WalrusH.SetActive(true);
                break;
        }
        endAd.gameObject.SetActive(false);
        UI.SetActive(false);
        EndScreen.gameObject.SetActive(true);
    }
    public void Restart()
    {
        ads.End();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Begin()
    {
        optionsUI.SetActive(false);
        shopUI.SetActive(false);
        MainMenuUI.SetActive(false);
        UI.SetActive(true);
        Music.volume = 0.8f;
        Music.GetComponent<MusicControl>().Music2();
        Time.timeScale = 1;
        MonoBehaviour[] scripts = player.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = true;
        }

    }

    //UPGRADES
    public void UpgradeSpeed()
    {
        UsedShop = true;
        PlayerPrefs.SetInt("Speed", PlayerPrefs.GetInt("Speed") + 150);
        SpeedText.text = "SPEED (" + (1+ PlayerPrefs.GetInt("Speed") / 150) + ")";
        PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - (15 + PlayerPrefs.GetInt("priceS")) );
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
        PlayerPrefs.SetInt("priceS", Mathf.FloorToInt(PlayerPrefs.GetInt("priceS")*1.4f + 5) );
        SpeedButton.GetComponentInChildren<TextMeshProUGUI>().text = (15 + PlayerPrefs.GetInt("priceS")).ToString();

        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceS")) SpeedButton.interactable = false;
        else SpeedButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceT")) TimeButton.interactable = false;
        else TimeButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 25 + PlayerPrefs.GetInt("priceC")) CarrotButton.interactable = false;
        else CarrotButton.interactable = true;
    }
    public void UpgradeTime()
    {
        UsedShop = true;
        PlayerPrefs.SetInt("Time", PlayerPrefs.GetInt("Time") + 1);
        TimeText.text = "TIME (" + (1 + PlayerPrefs.GetInt("Time")) + ")";
        PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - (15 + PlayerPrefs.GetInt("priceT")));
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
        PlayerPrefs.SetInt("priceT", Mathf.FloorToInt(PlayerPrefs.GetInt("priceT") * 1.4f + 5));
        TimeButton.GetComponentInChildren<TextMeshProUGUI>().text = (15 + PlayerPrefs.GetInt("priceT")).ToString();

        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceS")) SpeedButton.interactable = false;
        else SpeedButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceT")) TimeButton.interactable = false;
        else TimeButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 25 + PlayerPrefs.GetInt("priceC")) CarrotButton.interactable = false;
        else CarrotButton.interactable = true;
    }
    public void UpgradeCarrot()
    {
        UsedShop = true;
        PlayerPrefs.SetFloat("Carrot", PlayerPrefs.GetFloat("Carrot") + .5f);
        CarrotText.text = "CARROTS (" + (1 + PlayerPrefs.GetFloat("Carrot") /.5f) + ")";
        PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - (25 + PlayerPrefs.GetInt("priceC")));
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
        PlayerPrefs.SetInt("priceC", Mathf.FloorToInt(PlayerPrefs.GetInt("priceC") * 1.4f + 5));
        CarrotButton.GetComponentInChildren<TextMeshProUGUI>().text = (25 + PlayerPrefs.GetInt("priceC")).ToString();

        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceS")) SpeedButton.interactable = false;
        else SpeedButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 15 + PlayerPrefs.GetInt("priceT")) TimeButton.interactable = false;
        else TimeButton.interactable = true;
        if (PlayerPrefs.GetInt("Gold") < 25 + PlayerPrefs.GetInt("priceC")) CarrotButton.interactable = false;
        else CarrotButton.interactable = true;
    }

    public void AdGold()
    {
        PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") + 10);
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
    }

    public void AdContinue()
    {
        StopAllCoroutines();
        GameData.continueused = true;
        Time.timeScale = 1;
        MonoBehaviour[] scripts = player.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = true;
        }
        UI.SetActive(true);
        endAd.gameObject.SetActive(false);
        potion.StartCoroutine("Rabbits");
        Music.volume = 1f;
    }

    private IEnumerator Timer()
    {
        float pauseEndTime = Time.realtimeSinceStartup + .8f;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        TimerText.text = "2";
        pauseEndTime += 1f;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        TimerText.text = "1";
        pauseEndTime += 1f;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        EndAd2();
    }

    //options

    public void FlipJoystick()
    {
        if(PlayerPrefs.GetInt("Joystick")==0)
        {
            joystick.localPosition = new Vector2(815, joystick.localPosition.y);
            PlayerPrefs.SetInt("Joystick", 1);
        }
        else
        {
            joystick.localPosition = new Vector2(120, joystick.localPosition.y);
            PlayerPrefs.SetInt("Joystick", 0);
        }
        
    }
    public void ToggleMusic()
    {
        if (Music.enabled == false)
        {
            Music.enabled = true;
            PlayerPrefs.SetInt("Music", 0);
        }
        else
        {
            Music.enabled = false;
            PlayerPrefs.SetInt("Music", 1);
        }

    }
    public void ToggleSound()
    {
        if (Sound.enabled == false)
        {
            Sound.enabled = true;
            PlayerPrefs.SetInt("Sound", 0);
        }
        else
        {
            Sound.enabled = false;
            PlayerPrefs.SetInt("Sound", 1);
        }

    }
}


