﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class HUD : MonoBehaviour
{
    public TextMeshProUGUI timer;
    protected GameController gameController;
    protected Sounds Sound;
    public float t = 20f;
    private float temp;
    private bool clock=false;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        t += PlayerPrefs.GetInt("Time");
    }

    void Update()
    {
        if (t > 1)
        {
            t -= Time.deltaTime;
            timer.text = Mathf.Floor(t).ToString("r");
        }
        else if (t == -99)
            timer.text = "oo";
        if (Mathf.Floor(t) <= 0 && t!=-99)
            gameController.End();
        if(t<5 && t>0 && !clock)
        {
            Sound.Sound7();
            clock = true;
        }
            


    }

    public void Timer(float a)
    {
        if (GameData.Monster == 0)
            t += a;
        else
            temp += a;
        if (t > 5 && clock)
        {
            Sound.Stop();
            clock = false;
        }

    }

    public IEnumerator StopTime()
    {
        temp = t;
        t = -99;
        yield return new WaitForSeconds(9f);
        t = temp;
    }
}
