﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goat : MonoBehaviour
{
    protected Sounds Sound;
    protected PlayerControl Player;
    protected GameController gameController;
    private Animator anim;
    private Rigidbody2D rb2d;
    private bool Idle = true;
    private bool Chasing = false;
    private bool Stop = false;
    public Vector2 Target;
    public Vector2 ChaseTarget;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        anim = GetComponent<Animator>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        StartCoroutine("Patrol");
    }

    private void Update()
    {
        if (Idle)
        {
            rb2d.MovePosition(Vector2.MoveTowards(transform.position, Target, 2 * Time.deltaTime));
        }

        if (Mathf.Abs(transform.position.x - Player.transform.position.x) + Mathf.Abs(transform.position.y - Player.transform.position.y) < 8 && !Chasing && !Stop)
        {
            if (Idle)
                Sound.Sound11();
            StopCoroutine("Patrol");
            Idle = false;
            Chasing = true;
            ChaseTarget = Player.transform.position;
        }
        else if (!(Mathf.Abs(transform.position.x - Player.transform.position.x) + Mathf.Abs(transform.position.y - Player.transform.position.y) < 8) && !Chasing && !Stop)
            Idle = true;

        if (Chasing)
        {
            StartCoroutine("Stopping");
            rb2d.MovePosition(Vector2.MoveTowards(transform.position, ChaseTarget, 5 * Time.deltaTime));
            anim.SetBool("Walking", true);
        }

    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject == Player.gameObject)
        {
            gameController.ReplacePlayer();
        }
        else
        {
            col.gameObject.SendMessage("Die");

        }
        if (col.tag == "Cage" && Idle)
            OppositeTarget(ref Target);

    }

    private IEnumerator Patrol()
    {

        RandomTarget(ref Target);
        yield return new WaitForSeconds(10f);
        StartCoroutine("Patrol");
    }

    private Vector2 RandomTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(Random.Range(-240, 360), Random.Range(-120, 200));
        return Tar;
    }
    private Vector2 OppositeTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(-Tar.x, -Tar.y);
        return Tar;
    }

    private IEnumerator Stopping()
    {
        yield return new WaitForSeconds(1.5f);
        Chasing = false;
        Stop = true;
        anim.SetBool("Walking", false);
        yield return new WaitForSeconds(1.5f);
        Idle = true;
        Stop = false;
    }

}
