﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitFollow : MonoBehaviour
{
    protected PlayerControl Player;
    protected Sounds Sound;
    protected GameController gameController;
    private GameObject Target;
    private GameObject Rabbit;
    private Rigidbody2D rb2d;
    private CircleCollider2D col2D;
    private Animator anim;
    private SpriteRenderer sprite;
    private ParticleSystem particles;

    float speed = 9;

    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        col2D = gameObject.GetComponent<CircleCollider2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        particles = GetComponent<ParticleSystem>();

        Target = gameController.Rabbits[gameController.Rabbits.Count - 2];
        Rabbit = gameController.Rabbits[gameController.Rabbits.Count - 1];

        speed += (PlayerPrefs.GetInt("Speed") / 750);

        SkinChange();
    }

    void Update()
    {
        if (Target != null)
        {
            if (Mathf.Abs(transform.position.x - Target.transform.position.x) + Mathf.Abs(transform.position.y - Target.transform.position.y) > 2)
            {
                rb2d.MovePosition(Vector2.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime));
                anim.SetBool("Walking", true);
            }
            else if (Mathf.Abs(transform.position.x - Target.transform.position.x) + Mathf.Abs(transform.position.y - Target.transform.position.y) < 2)
            {
                anim.SetBool("Walking", false);
            }

            if (transform.position.x - Target.transform.position.x > 0)
                sprite.flipX = true;
            else
                sprite.flipX = false;

        }
        else
            StartCoroutine("ReTarget");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag=="Obstacle")
        {
            StartCoroutine("Collision");
        }
    }


    public IEnumerator ReTarget()
    {
        yield return new WaitForSeconds(.0f);
        Debug.Log("1");

        int index = gameController.Rabbits.IndexOf(Rabbit);


        if (Target == null)
        {
            Debug.Log("2");
            Target = gameController.Rabbits[index - 1];
            Debug.Log(Target);
        }
        else if (gameController.Rabbits[index + 1] != null)
            gameController.Rabbits[index + 1].SendMessage("ReTarget");

    }

    public IEnumerator Die()
    {
        Debug.Log("3");
        int Index = gameController.Rabbits.IndexOf(Rabbit);
        if (Index + 1 < gameController.Rabbits.Count)
            gameController.Rabbits[Index + 1].SendMessage("ReTarget");
        yield return new WaitForSeconds(.01f);
        gameController.Rabbits.Remove(this.gameObject);
        Object Blood = Resources.Load("Blood") as GameObject;
        GameObject clone = Instantiate(Blood, Vector3.zero, Quaternion.identity) as GameObject;
        clone.transform.position = transform.position;
        Sound.Sound13();
        Destroy(this.gameObject,.1f);

    }

    public IEnumerator Collision()
    {
        col2D.isTrigger = false;
        yield return new WaitForSeconds(.3f);
        col2D.isTrigger = true;
    }

    void SkinChange()
    {
        switch (PlayerPrefs.GetInt("SelectedSkin"))
        {
            case 0:
                sprite.sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                break;
            case 1:
                sprite.sprite = Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                break;
            case 2:
                sprite.sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                break;
            case 3:
                sprite.sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                break;
            case 4:
                sprite.sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                break;
            case 5:
                sprite.sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                break;
            case 6:
                sprite.sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                break;
        }
    }
}