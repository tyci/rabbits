﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    protected Transform CameraParent;
    protected Camera maincamera;
    protected PlayerControl Player;
    protected MonsterControl Monster;
    void Start()
    {
        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        CameraParent = GetComponentInParent<Transform>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        Monster = GameObject.FindGameObjectWithTag("monster2").GetComponent<MonsterControl>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameData.Monster==0)
            CameraParent.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -10);
        else
            CameraParent.transform.position = new Vector3(Monster.transform.position.x, Monster.transform.position.y, -10);
    }
}
