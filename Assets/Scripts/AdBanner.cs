﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using System.Diagnostics;
public class AdBanner : MonoBehaviour
{
    StackTrace stackTrace;
    public string gameId = "3645927";
    public string placementId = "bannerPlacement";
    public bool testMode = false;

    void Start()
    {
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Initialize(gameId, testMode);

    }

    public IEnumerator ShowBannerWhenReady()
    {
        stackTrace = new StackTrace();
        print("stackTrace !! " + stackTrace.GetFrame(1).GetMethod().Name);
        while (!Advertisement.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(placementId);
    }
    public void Stop()
    {
        Advertisement.Banner.Hide();
    }
}
