﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SkinController : MonoBehaviour
{
    public GameObject RabbitIMG;
    public GameObject ChickIMG;
    public GameObject DuckIMG;
    public GameObject GorrilaIMG;
    public GameObject PenguinIMG;
    public GameObject SlothIMG;
    public GameObject WalrusIMG;

    public TextMeshProUGUI NameTxt;
    public TextMeshProUGUI gold;
    public Button BuyButton;
    public Button SelectButton;
    public Sprite ChickSprite;

    public PauseMenu Menus;
    int Number = 0;
    bool[] selected = new bool[7];
    protected PlayerControl Player;

    public List<GameObject> Skins = new List<GameObject>();
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        Skins.Add(RabbitIMG);
        Skins.Add(ChickIMG);
        Skins.Add(DuckIMG);
        Skins.Add(PenguinIMG);
        Skins.Add(GorrilaIMG);
        Skins.Add(SlothIMG);
        Skins.Add(WalrusIMG);

        selected[PlayerPrefs.GetInt("SelectedSkin")] = true;
        if (PlayerPrefs.GetInt("ChickS") == 1) Destroy((Skins[1].GetComponentInChildren<SpriteRenderer>()).gameObject);
        if (PlayerPrefs.GetInt("DuckS") == 1) Destroy((Skins[2].GetComponentInChildren<SpriteRenderer>()).gameObject);
        if (PlayerPrefs.GetInt("PenguinS") == 1) Destroy((Skins[3].GetComponentInChildren<SpriteRenderer>()).gameObject);
        if (PlayerPrefs.GetInt("GorillaS") == 1) Destroy((Skins[4].GetComponentInChildren<SpriteRenderer>()).gameObject);
        if (PlayerPrefs.GetInt("SlothS") == 1) Destroy((Skins[5].GetComponentInChildren<SpriteRenderer>()).gameObject);
        if (PlayerPrefs.GetInt("WalrusS") == 1) Destroy((Skins[6].GetComponentInChildren<SpriteRenderer>()).gameObject);
    }
    private void Update()
    {
        gold.text = PlayerPrefs.GetInt("Gold").ToString();

        switch (Number)
        {
            case 0:
                Skins[6].SetActive(false);
                Skins[0].SetActive(true);
                Skins[1].SetActive(false);
                NameTxt.text = "RABBIT";
                BuyButton.gameObject.SetActive(false);
                SelectButton.gameObject.SetActive(true);
                if (selected[0] == true) SelectButton.interactable = false;
                else SelectButton.interactable = true;
                break;
            case 1:
                Skins[0].SetActive(false);
                Skins[1].SetActive(true);
                Skins[2].SetActive(false);
                NameTxt.text = "CHICK";
                if (PlayerPrefs.GetInt("ChickS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[1] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (50)";
                    if (PlayerPrefs.GetInt("Gold") < 50) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                    break;
            case 2:
                Skins[1].SetActive(false);
                Skins[2].SetActive(true);
                Skins[3].SetActive(false);
                NameTxt.text = "DUCK";
                if (PlayerPrefs.GetInt("DuckS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[2] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (50)";
                    if (PlayerPrefs.GetInt("Gold") < 50) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                break;
            case 3:
                Skins[2].SetActive(false);
                Skins[3].SetActive(true);
                Skins[4].SetActive(false);
                NameTxt.text = "PENGUIN";
                if (PlayerPrefs.GetInt("PenguinS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[3] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (75)";
                    if (PlayerPrefs.GetInt("Gold") < 75) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                break;
            case 4:
                Skins[3].SetActive(false);
                Skins[4].SetActive(true);
                Skins[5].SetActive(false);
                NameTxt.text = "GORILLA";
                if (PlayerPrefs.GetInt("GorillaS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[4] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (75)";
                    if (PlayerPrefs.GetInt("Gold") < 75) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                break;
            case 5:
                Skins[4].SetActive(false);
                Skins[5].SetActive(true);
                Skins[6].SetActive(false);
                NameTxt.text = "SLOTH";
                if (PlayerPrefs.GetInt("SlothS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[5] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (100)";
                    if (PlayerPrefs.GetInt("Gold") < 100) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                break;
            case 6:
                Skins[5].SetActive(false);
                Skins[6].SetActive(true);
                Skins[0].SetActive(false);
                NameTxt.text = "WALRUS";
                if (PlayerPrefs.GetInt("WalrusS") == 1)
                {
                    BuyButton.gameObject.SetActive(false);
                    SelectButton.gameObject.SetActive(true);
                    if (selected[6] == true) SelectButton.interactable = false;
                    else SelectButton.interactable = true;
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                    SelectButton.gameObject.SetActive(false);
                    BuyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Buy (100)";
                    if (PlayerPrefs.GetInt("Gold") < 100) BuyButton.interactable = false;
                    else BuyButton.interactable = true;
                }
                break;
        }
        if (Number == 7) Number = 0;
        if (Number == -1) Number = 6;
    }

    public void Next()
    {
        Number++;
    }
    public void Previous()
    {
        Number--;
    }

    public void Buy()
    {
        Destroy((Skins[Number].GetComponentInChildren<SpriteRenderer>()).gameObject);
        switch (Number)
        {
            case 1:
                PlayerPrefs.SetInt("ChickS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 50);
                break;
            case 2:
                PlayerPrefs.SetInt("DuckS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 50);
                break;
            case 3:
                PlayerPrefs.SetInt("PenguinS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 75);
                break;
            case 4:
                PlayerPrefs.SetInt("GorillaS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 75);
                break;
            case 5:
                PlayerPrefs.SetInt("SlothS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 100);
                break;
            case 6:
                PlayerPrefs.SetInt("WalrusS", 1);
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") - 100);
                break;
        }
        Menus.UsedShop = true;

    }
    public void Select()
    {
        for (int i = 0; i < 6; i++) selected[i] = false;
        selected[Number] = true;
        PlayerPrefs.SetInt("SelectedSkin", Number);
        Menus.UsedShop = true;
    }
}
