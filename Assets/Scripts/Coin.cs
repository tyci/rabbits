﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    protected PlayerControl Player;
    protected Sounds Sound;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == Player.gameObject || col.gameObject.tag == "monster2")
        {
            Sound.Sound1();
            GameData.Coins += 1;
            Destroy(gameObject);
        }

    }
}