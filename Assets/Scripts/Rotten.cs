﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotten : MonoBehaviour
{
    protected PlayerControl Player;
    protected GameController gameController;
    protected Sounds Sound;
    float temp;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == Player.gameObject)
        {
            Sound.Sound8();
            Sound.Sound5();
            temp =Player.speed;
            gameController.StartCoroutine("Buff", .2f);
            Destroy(gameObject);
        }

    }
}
