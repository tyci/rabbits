﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    protected PlayerControl Player;
    public TextMeshProUGUI gold;
    public TextMeshProUGUI Score;
    public TextMeshProUGUI High;
    public TextMeshProUGUI PauseScore;
    public PauseMenu Menus;
    public AudioSource Music;
    public Ads ad;
    public float t = 0;
    private Vector2 pos;

    public List<GameObject> Rabbits = new List<GameObject>();
    void Awake()
    {
        ad = GetComponent<Ads>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        Rabbits.Add(Player.gameObject);
    }
    private void Start()
    {
        GameData.end = false;
        GameData.Buff = 1;
        GameData.Monster = 0;
        GameData.continueused = false;
        Music = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        High.text = PlayerPrefs.GetFloat("High Score").ToString("R") + " points";
        gold.text = PlayerPrefs.GetInt("Gold").ToString();
        StartCoroutine("Carrots");
        StartCoroutine("Rotten");
        StartCoroutine("Gold");
        StartCoroutine("Diamond");
        StartCoroutine("Hat");
        StartCoroutine("Potion");
        Wolves();
        Cows();
        Goats();
    }

    void Update()
    {
        t += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Delete))
            PlayerPrefs.DeleteAll();
        PauseScore.text = Mathf.Floor(t).ToString("R") + " points";
    }
    public void ReplacePlayer()
    {
        if (Rabbits.Count > 1)
        {
            Debug.Log("zmiana");
            GameData.Buff = 1;
            Player.GetComponent<SpriteRenderer>().color = Color.white;
            pos=Player.transform.position;
            Player.transform.position = Rabbits[1].transform.position;
            Rabbits[1].transform.position=pos;
            Rabbits[1].SendMessage("Die");
        }
        else
        {
            End();
        }

    }
    public void End()
    {
        Music.volume = 0.4f;
        if (GameData.continueused == false)
        {
            GameData.end = true;
            Time.timeScale = 0;
            MonoBehaviour[] scripts = Player.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour script in scripts)
            {
                script.enabled = false;
            }
            Menus.EndAd();
        }
        else
        {
            Score.text = Mathf.Floor(t).ToString("R") + " points";
            if (t > PlayerPrefs.GetFloat("High Score"))
                PlayerPrefs.SetFloat("High Score", Mathf.Floor(t));
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold") + GameData.Coins);
            PlayerPrefs.Save();
            gold.text = PlayerPrefs.GetInt("Gold").ToString();
            GameData.Coins = 0;
            Time.timeScale = 0;
            MonoBehaviour[] scripts = Player.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour script in scripts)
            {
                script.enabled = false;
            }
            ad.End();
            Menus.End();
        }
    }
    private IEnumerator Carrots()
    {
       while (GameObject.FindGameObjectsWithTag("Carrot").Length < 70)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if(caster(Tar, 3)==0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 4)
            {
                Object prefab = Resources.Load("Carrot") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
        yield return new WaitForSeconds(5f);
        StartCoroutine("Carrots");
    }
    private IEnumerator Rotten()
    {
        while (GameObject.FindGameObjectsWithTag("Rotten").Length < 15)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 4)
            {
                Object prefab = Resources.Load("Rotten") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
        yield return new WaitForSeconds(5f);
        StartCoroutine("Rotten");
    }
    private IEnumerator Gold()
    {
        while (GameObject.FindGameObjectsWithTag("Gold").Length < 20)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("coin") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
        yield return new WaitForSeconds(5f);
        StartCoroutine("Gold");
    }
    private IEnumerator Diamond()
    {
        while (GameObject.FindGameObjectsWithTag("Diamond").Length < 3)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("diamond") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
        yield return new WaitForSeconds(25f);
        StartCoroutine("Diamond");
    }
    private IEnumerator Hat()
    {
        while (GameObject.FindGameObjectsWithTag("Hat").Length < 6)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("Hat") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
                Destroy(clone, 25);
            }
        }
        yield return new WaitForSeconds(25f);
        StartCoroutine("Hat");
    }
    private IEnumerator Potion()
    {

        while (GameObject.FindGameObjectsWithTag("Potion").Length < 2)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("Potion") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
        yield return new WaitForSeconds(30);
        StartCoroutine("Potion");
    }
    public void Wolves()
    {
        while (GameObject.FindGameObjectsWithTag("Wolf").Length < 25)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("Wolf") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
    }
    public void Goats()
    {
        while (GameObject.FindGameObjectsWithTag("Goat").Length < 40)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("Goat") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
    }
    public void Cows()
    {
        while (GameObject.FindGameObjectsWithTag("Cow").Length < 20)
        {
            Vector2 Tar = new Vector2(Random.Range(-60, 90), Random.Range(-30, 50));
            if (caster(Tar, 3) == 0 && Mathf.Abs(Tar.x - Player.transform.position.x) + Mathf.Abs(Tar.y - Player.transform.position.y) > 8)
            {
                Object prefab = Resources.Load("Cow") as GameObject;
                GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                clone.transform.position = Tar;
            }
        }
    }

    int caster(Vector2 center, float radius)
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].tag == "Obstacle")
            {
                return 1;
            }
            i++;
        }
        return 0;
    }

   public IEnumerator Buff(float x)
    {
        GameData.Buff = x;
        Player.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.white, Color.green, .5f);
        yield return new WaitForSeconds(2.5f);
        GameData.Buff = 1;
        Player.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public IEnumerator RabbitsTP()
    {
        yield return new WaitForSeconds(.01f);
        for (int i = 1; i < Rabbits.Count; i++)
            Rabbits[i].transform.position = Player.transform.position;
        yield return new WaitForSeconds(9f);
        for (int i = 1; i < Rabbits.Count; i++)
            Rabbits[i].transform.position = Player.transform.position;
    }

}

