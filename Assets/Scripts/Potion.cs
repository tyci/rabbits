﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{
    protected PlayerControl Player;
    protected GameController gameController;
    protected MonsterControl Monster;
    protected HUD hud;
    protected Sounds Sound;
    Vector2 PlrXY, MnstrXY;

    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        Monster = GameObject.FindGameObjectWithTag("monster2").GetComponent<MonsterControl>();
        hud = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HUD>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            StartCoroutine("Rabbits");
        }

    }

    public IEnumerator Rabbits()
    {
        Debug.Log("monster");
        transform.position = new Vector2(MnstrXY.x - 100, MnstrXY.y - 100);
        Monster.StartCoroutine("Size");
        Monster.StartCoroutine("Grow");
        GameData.Monster = 1;
        PlrXY = Player.transform.position;
        MnstrXY = Monster.transform.position;
        Monster.transform.position = PlrXY;
        Player.transform.position = MnstrXY;
        hud.StartCoroutine("StopTime");
        gameController.StartCoroutine("RabbitsTP");
        Sound.Sound3();

        yield return new WaitForSeconds(8f);

        Sound.Sound4();
        yield return new WaitForSeconds(1f);
        GameData.Monster = 0;
        Player.transform.position = Monster.transform.position;
        Monster.transform.position = MnstrXY;
        gameController.Wolves();
        gameController.Cows();
        gameController.Goats();
        Destroy(gameObject);
    }

}