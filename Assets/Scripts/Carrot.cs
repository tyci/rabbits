﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
public class Carrot : MonoBehaviour
{
    protected Sounds Sound;
    protected PlayerControl Player;
    public TextMeshProUGUI carrots;
    protected HUD hud;
    protected GameController gameController;
    float Time = 2;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        hud = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HUD>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        Time += PlayerPrefs.GetFloat("Carrot");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == Player.gameObject || col.gameObject.tag=="monster2")
        {
            Sound.Sound8();
            Object prefab = Resources.Load("Rabbits") as GameObject;
            GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
            if(GameData.Monster==0)
                clone.transform.position = transform.position;
            else
                clone.transform.position = Player.transform.position;
            gameController.Rabbits.Add(clone);
            hud.Timer(Time);

            Object prefab2 = Resources.Load("Carrot Text") as GameObject;
            GameObject clone2 = Instantiate(prefab2, Vector3.zero, Quaternion.identity) as GameObject;
            clone2.transform.position = transform.position;
            carrots = clone2.GetComponent<TextMeshProUGUI>();
            carrots.text = "+"+Time.ToString();
            Destroy(clone2, 1);
            Destroy(gameObject);
        }
        else if (col.tag == "Cow")
        {
            Destroy(gameObject);
        }
    }

}