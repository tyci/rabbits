﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour
{
    private AudioSource Music;
    public AudioClip audio1;
    public AudioClip audio2;
    void Start()
    {
        Music = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
    }
    public void Music1()
    {
        Music.Stop();
        Music.loop = true;
        Music.clip = audio1;
        Music.Play();
    }
    public void Music2()
    {
        Music.Stop();
        Music.loop = true;
        Music.clip = audio2;
        Music.Play();
    }

}
