﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{
    private AudioSource Sound;
    public AudioClip audio1;
    public AudioClip audio2;
    public AudioClip audio3;
    public AudioClip audio4;
    public AudioClip audio5;
    public AudioClip audio6;
    public AudioClip audio7;
    public AudioClip audio8;
    public AudioClip audio9;
    public AudioClip audio10;
    public AudioClip audio11;
    public AudioClip audio12;
    public AudioClip audio13;
    public AudioClip audio14;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<AudioSource>();
    }

    public void Sound1()
    {
        Sound.PlayOneShot(audio1);
    }
    public void Sound2()
    {
        Sound.PlayOneShot(audio2);
    }
    public void Sound3()
    {
        Sound.PlayOneShot(audio3);
    }
    public void Sound4()
    {
        Sound.PlayOneShot(audio4);
    }
    public void Sound5()
    {
        Sound.PlayOneShot(audio5);
    }
    public void Sound6()
    {
        Sound.PlayOneShot(audio6);
    }
    public void Sound7()
    {
        Sound.PlayOneShot(audio7);
    }
    public void Sound8()
    {
        Sound.PlayOneShot(audio8);
    }
    public void Sound9()
    {
        Sound.PlayOneShot(audio9);
    }
    public void Sound10()
    {
        Sound.PlayOneShot(audio10);
    }
    public void Sound11()
    {
        Sound.PlayOneShot(audio11);
    }
    public void Sound12()
    {
        Sound.PlayOneShot(audio12);
    }
    public void Sound13()
    {
        Sound.PlayOneShot(audio13);
    }

    public void Sound14()
    {
        Sound.PlayOneShot(audio14);
    }


    public void Stop()
    {
        Sound.Stop();
    }
}
