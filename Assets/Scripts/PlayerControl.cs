﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public Joystick joystick;
    public float speed = 50f;
    public float maxSpeed = 3;
    private Animator anim;
    private SpriteRenderer Sprite;
    private void Awake()
    {
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<Joystick>();
        Sprite = GetComponent<SpriteRenderer>();
        SkinChange();
    }
    void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();


        speed += PlayerPrefs.GetInt("Speed");


    }

    void Update()
    {
        //turning around
        if (joystick.Horizontal< -0.01f)
        {
            Sprite.flipX = true;
        }
        if (joystick.Horizontal > 0.01f)
        {
            Sprite.flipX = false;
        }
       
    }

    void FixedUpdate()
    {
        rb2d.AddForce(new Vector2(joystick.Horizontal * speed * GameData.Buff * Time.deltaTime, joystick.Vertical * speed * GameData.Buff * Time.deltaTime));
        if(joystick.Horizontal!=0 || joystick.Vertical!=0)
            anim.SetBool("Walking", true);
        else
            anim.SetBool("Walking", false);
    }

    void SkinChange()
    {
        switch (PlayerPrefs.GetInt("SelectedSkin"))
        {
            case 0:
                Sprite.sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                break;
            case 1:
                Sprite.sprite = Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite =  Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                break;
            case 2:
                Sprite.sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                break;
            case 3:
                Sprite.sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                break;
            case 4:
                Sprite.sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                break;
            case 5:
                Sprite.sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                break;
            case 6:
                Sprite.sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                break;
        }
    }
}

   