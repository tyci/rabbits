﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterControl : MonoBehaviour
{
    protected Sounds Sound;
    private Rigidbody2D rb2d;
    public Joystick joystick;
    public float speed = 50f;
    public float maxSpeed = 3;
    private Animator anim;
    private SpriteRenderer sprite;
    private void Awake()
    {
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<Joystick>();
    }
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();

        speed += PlayerPrefs.GetInt("Speed");
        SkinChange();
    }

    void Update()
    {
        //turning around
        if (joystick.Horizontal < -0.01f)
        {
            sprite.flipX = true;
        }
        if (joystick.Horizontal > 0.01f)
        {
            sprite.flipX = false;
        }

    }

    void FixedUpdate()
    {
        rb2d.AddForce(new Vector2(joystick.Horizontal * speed * GameData.Buff * Time.deltaTime, joystick.Vertical * speed * GameData.Buff * Time.deltaTime));
        if (joystick.Horizontal != 0 || joystick.Vertical != 0)
            anim.SetBool("Walking", true);
        else
            anim.SetBool("Walking", false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Wolf" || collision.gameObject.tag == "Goat" || collision.gameObject.tag == "Cow")
        {
            Sound.Sound8();
            Sound.Sound14();
            Object Blood = Resources.Load("Blood") as GameObject;
            GameObject clone = Instantiate(Blood, Vector3.zero, Quaternion.identity) as GameObject;
            clone.transform.position = collision.transform.position;
            Destroy(collision.gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Rotten" || col.gameObject.tag == "Hat" || col.gameObject.tag == "Potion")
        {
            Sound.Sound8();
            Destroy(col.gameObject);
        }
    }
    public IEnumerator Size()
    {
        yield return new WaitForSeconds(8f);
        anim.SetBool("Size", true);
        yield return new WaitForSeconds(2f);
        anim.SetBool("Size", false);
    }
    public IEnumerator Grow()
    {
        anim.SetBool("Grow", true);
        yield return new WaitForSeconds(1f);
        anim.SetBool("Grow", false);
    }
    void SkinChange()
    {
        switch (PlayerPrefs.GetInt("SelectedSkin"))
        {
            case 0:
                sprite.sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/rabbit", typeof(Sprite)) as Sprite;
                break;
            case 1:
                sprite.sprite = Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/chick", typeof(Sprite)) as Sprite;
                break;
            case 2:
                sprite.sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/duck", typeof(Sprite)) as Sprite;
                break;
            case 3:
                sprite.sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/penguin", typeof(Sprite)) as Sprite;
                break;
            case 4:
                sprite.sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/gorilla", typeof(Sprite)) as Sprite;
                break;
            case 5:
                sprite.sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/sloth", typeof(Sprite)) as Sprite;
                break;
            case 6:
                sprite.sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Skins/walrus", typeof(Sprite)) as Sprite;
                break;


        }
    }
        }
