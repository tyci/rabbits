﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : MonoBehaviour
{
    protected PlayerControl Player;
    protected GameController gameController;
    protected Sounds Sound;

    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == Player.gameObject)
        {
            Sound.Sound2();
            StartCoroutine("Rabbits");
        }

    }

    public IEnumerator Rabbits()
    {
        Object prefab = Resources.Load("Rabbits") as GameObject;
        GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        clone.transform.position = transform.position;
        gameController.Rabbits.Add(clone);
        yield return new WaitForSeconds(.1f);
        clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        clone.transform.position = transform.position;
        gameController.Rabbits.Add(clone);
        yield return new WaitForSeconds(.1f);
        clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        clone.transform.position = transform.position;
        gameController.Rabbits.Add(clone);
        Destroy(gameObject);
    }
}
