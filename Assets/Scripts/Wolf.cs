﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{
    protected Sounds Sound;
    protected PlayerControl Player;
    protected GameController gameController;
    private Rigidbody2D rb2d;
    private  bool Idle  =true;
    public Vector2 Target;
    private Animator anim;
    void Start()
    {
        Sound = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        gameController= GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        StartCoroutine("Patrol");
    }

    private void Update()
    {
        if(Idle)
            rb2d.MovePosition(Vector2.MoveTowards(transform.position, Target, 2 * Time.deltaTime));

        if (Mathf.Abs(transform.position.x - Player.transform.position.x) + Mathf.Abs(transform.position.y - Player.transform.position.y) < 8)
        {
            if(Idle)
                Sound.Sound10();
            Idle = false;
            rb2d.MovePosition(Vector2.MoveTowards(transform.position, Player.transform.position, 4 * Time.deltaTime));
            anim.SetBool("Walking", true);
        }
        else
        {
            Idle = true;
            anim.SetBool("Walking", false);
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject == Player.gameObject)
        {
            gameController.ReplacePlayer();
        }
        else
        {
            col.gameObject.SendMessage("Die");

        }
        if(col.tag=="Cage"&& Idle)
            OppositeTarget(ref Target);
    }


    private IEnumerator Patrol()
    {

        RandomTarget(ref Target);
        yield return new WaitForSeconds(10f);
        StartCoroutine("Patrol");
    }

    private Vector2 RandomTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(Random.Range(-240, 360),Random.Range(-120, 200));
        return Tar;
    }
    private Vector2 OppositeTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(-Tar.x, -Tar.y);
        return Tar;
    }

}
