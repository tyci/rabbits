﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour
{
    protected Sounds Sound;
    protected PlayerControl Player;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool Idle = true;
    public Vector2 Target;
    private bool playing = false;
    void Start()
    {
        Sound = GetComponentInChildren<Sounds>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        StartCoroutine("Patrol");
    }
    private void Update()
    {
        if (Idle)
            transform.position = Vector2.MoveTowards(transform.position, Target, 2 * Time.deltaTime);

        if (caster(transform.position, 6) == 1)
        {
            Idle = false;
            rb2d.MovePosition(Vector2.MoveTowards(transform.position, Target, 3 * Time.deltaTime));
            anim.SetBool("Walking", true);
        }
        else
        {
            Idle = true;
            anim.SetBool("Walking", false);
        }

        if (Mathf.Abs(transform.position.x - Player.transform.position.x) + Mathf.Abs(transform.position.y - Player.transform.position.y) < 5 && !playing)
        {
            StartCoroutine("Moo");
        }

    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Cage" && Idle)
            OppositeTarget(ref Target);
    }

    private IEnumerator Moo()
    {
        Sound.Sound1();
        playing = true;
        yield return new WaitForSeconds(3f);
        playing = false;
    }
    private IEnumerator Patrol()
    {

        RandomTarget(ref Target);
        yield return new WaitForSeconds(10f);
        StartCoroutine("Patrol");
    }

    private Vector2 RandomTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(Random.Range(-240, 360), Random.Range(-120, 200));
        return Tar;
    }
    private Vector2 OppositeTarget(ref Vector2 Tar)
    {
        Tar = new Vector2(-Tar.x, -Tar.y);
        return Tar;
    }

    int caster(Vector2 center, float radius)
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].tag == "Carrot")
            {
                Target = hitColliders[i].transform.position;
                return 1;
            }
            i++;
        }
        return 0;
    }

}
