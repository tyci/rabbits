﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
public class Ads : MonoBehaviour, IUnityAdsListener
{
    string gameId = "3645927";
    string myPlacementId = "rewardedVideo";
    bool testMode = false;
    public PauseMenu menu;
    public Button button1;
    public Button button2;

    void Start()
    {
       menu = GameObject.FindGameObjectWithTag("Canvas").GetComponent<PauseMenu>();
       button1 = GetComponent<Button>();
       button2 = GetComponent<Button>();
        //button1.interactable = Advertisement.IsReady(myPlacementId);
        //button2.interactable = Advertisement.IsReady(myPlacementId);
        //if (myButton) myButton.onClick.AddListener(ShowRewardedVideo);
        End();
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, true);
    }

    // Implement a function for showing a rewarded video ad:
    public void ShowRewardedVideo()
    {
        Advertisement.Show(myPlacementId);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, activate the button: 
        if (placementId == myPlacementId)
        {
            button1.interactable = true;
            button2.interactable = true;
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            if (GameData.end == false)
            {
                menu.AdGold();
                print("gold");
            }
            else
            {
                print("rev");
                menu.AdContinue();
            }
        }
        else if (showResult == ShowResult.Skipped)
        {
            print("-");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        GameObject.FindGameObjectWithTag("Canvas").GetComponent<PauseMenu>().StopAllCoroutines();
    }
    public void End()
    {
        Advertisement.RemoveListener(this);
    }
}
